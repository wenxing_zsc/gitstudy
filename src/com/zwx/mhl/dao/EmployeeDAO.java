package com.zwx.mhl.dao;

import com.zwx.mhl.domain.Employee;

/**
 * @author 周文星
 * @version 1.0
 */
public class EmployeeDAO extends BasicDAO<Employee> {
    //这里还可以写特有的操作
}
