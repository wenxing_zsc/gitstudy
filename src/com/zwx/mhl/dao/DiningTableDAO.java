package com.zwx.mhl.dao;

import com.zwx.mhl.domain.DiningTable;

/**
 * @author 周文星
 * @version 1.0
 */
public class DiningTableDAO extends BasicDAO<DiningTable> {
    //若有特有操作，可写在此处

}
