package com.zwx.mhl.service;

import com.zwx.mhl.dao.EmployeeDAO;
import com.zwx.mhl.domain.Employee;

/**
 * @author 周文星
 * @version 1.0
 * 该类完成对employee表的各种操作（通过调用EmployeeDAO对象完成）
 */
public class EmployeeService {

    //定义一个EmployeeDAO 属性
    private EmployeeDAO employeeDAO = new EmployeeDAO();

    //方法，根据empId 和 pwd 返回一个Employee对象
//    如果查询不到，就返回null
    public Employee getEmployeeByIdAndPwd(String empId, String pwd) {

        return employeeDAO.querySingle("select * from employee where empId = ? and pwd = md5(?)",Employee.class, empId, pwd);
    }
}
