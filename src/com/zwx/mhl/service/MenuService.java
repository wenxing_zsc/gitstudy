package com.zwx.mhl.service;

import com.zwx.mhl.dao.MenuDAO;
import com.zwx.mhl.domain.Menu;

import java.util.List;

/**
 * @author 周文星
 * @version 1.0
 * 完成对menu表的各种操作（通过调用MenuDAO）
 */
public class MenuService {

    private MenuDAO menuDAO = new MenuDAO();

    //返回所有菜品,返回给界面
    public List<Menu> list() {
        return menuDAO.queryMulti("select * from menu", Menu.class);
    }

    //根据id，返回Menu对象
    public Menu getMenuById(int id) {
        return menuDAO.querySingle("select * from menu where id=?", Menu.class, id);
    }
}
