package com.zwx.mhl.service;

import com.zwx.mhl.dao.BillDAO;
import com.zwx.mhl.domain.Bill;

import java.util.List;
import java.util.UUID;

/**
 * @author 周文星
 * @version 1.0
 * 处理和账单相关的业务逻辑
 */
public class BillService {

    private BillDAO billDAO = new BillDAO();
    private MenuService menuService = new MenuService();
    private DiningTableService diningTableService = new DiningTableService();

    //思考
    //编写点餐的方法
    //1.生成账单
    //2.需要更新对应餐桌的状态
    //3.如果成功返回true，否则返回false
    public boolean orderMenu(int menuId, int nums,int diningTableId) {
        //生成一个账单号，UUID
        String billId = UUID.randomUUID().toString();
        Double money = menuService.getMenuById(menuId).getPrice() * nums;

        //将账单生成到bill表，要求直接计算账单金额
        int update = billDAO.update("insert into bill values(null,?,?,?,?,?,now(),'未结账')",
                billId, menuId, nums, money, diningTableId);

        if (update <= 0) {
            return false;
        }

        //需要更新对应餐桌的状态
        return diningTableService.updateDiningTableState(diningTableId, "就餐中");
    }

    //返回所有的账单，提供给View调用
    public List<Bill> list() {
        return billDAO.queryMulti("select * from bill", Bill.class);
    }

    //查看某个餐桌是否有未结账的账单
    public boolean hasPayBillByDiningTableId(int diningTableId) {

        Bill bill =
                billDAO.querySingle("select * from bill where diningTableId=? and state='未结账' limit 0,1", Bill.class, diningTableId);
        return bill != null;
    }

    //完成结账[如果餐桌存在，并且该餐桌有未结账的账单]
    public boolean payBill(int diningTableId, String payMode) {

        //1.修改bill表
        int update =
                billDAO.update("update bill set state=? where diningTableId=? and state='未结账'", payMode, diningTableId);

        if (update <= 0) {
            return false;
        }

        //2.修改diningTable表
        //注意：不要直接在这里操作，而一个调用DiningTableService 方法
        if (!diningTableService.updateDiningTableToFree(diningTableId,"空")) {
            return false;
        }

        return true;
    }
}
